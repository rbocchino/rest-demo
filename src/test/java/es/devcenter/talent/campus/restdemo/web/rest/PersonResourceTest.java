package es.devcenter.talent.campus.restdemo.web.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.devcenter.talent.campus.restdemo.AssertUtils;
import es.devcenter.talent.campus.restdemo.dto.PersonPayload;
import es.devcenter.talent.campus.restdemo.enums.Sex;
import es.devcenter.talent.campus.restdemo.model.Person;
import es.devcenter.talent.campus.restdemo.repository.PersonRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class PersonResourceTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private PersonRepository personRepository;

    private SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");


    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private <T> T mapFromJson(String json, Class<T> clazz)
            throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, clazz);
    }

    @Before
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    @Test
    public void testGetAllPerson() throws Exception {
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get("/api/person/")
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        Assert.assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
        String content = mvcResult.getResponse().getContentAsString();
        System.out.println(content);
        PersonPayload[] response = this.mapFromJson(content, PersonPayload[].class);

        List<Integer> real = Arrays.stream(response).map(personPayload -> personPayload.getIdPerson()).collect(Collectors.toList());
        AssertUtils.assertEquals(Arrays.asList(1,2), real);
    }

    @Test
    public void testGetById() throws Exception {
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get("/api/person/{id}/",1)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        Assert.assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
        String content = mvcResult.getResponse().getContentAsString();
        PersonPayload response = this.mapFromJson(content, PersonPayload.class);

        Assert.assertEquals(Integer.valueOf(1), response.getIdPerson());
        Assert.assertEquals("00000000T", response.getDocument());
        Assert.assertEquals("Juan Moreno Garcia", response.getFullName());
        Assert.assertNull(response.getDescription());
        Assert.assertEquals("16/09/1980",  this.formater.format(response.getBirthDate()));
        Assert.assertEquals(Sex.MALE, Sex.lookup(response.getSex()));
    }

    @Test
    public void testInsert() throws Exception {
        PersonPayload request = new PersonPayload();
        request.setFullName("Ana Lopez");
        request.setDocument("12345678T");
        request.setSex(Sex.FAMALE.getIdSex());
        request.setDescription("desc 3");
        request.setBirthDate(this.formater.parse("13/08/1975"));

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.post("/api/person/")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(this.mapToJson(request))).andReturn();
        Assert.assertEquals(HttpStatus.CREATED.value(), mvcResult.getResponse().getStatus());
        PersonPayload response = this.mapFromJson(mvcResult.getResponse().getContentAsString(), PersonPayload.class);

        //check payload
        Assert.assertEquals("12345678T", response.getDocument());
        Assert.assertEquals("Ana Lopez", response.getFullName());
        Assert.assertEquals("desc 3",response.getDescription());
        Assert.assertEquals("13/08/1975",  this.formater.format(response.getBirthDate()));
        Assert.assertEquals(Sex.FAMALE, Sex.lookup(response.getSex()));

        //check in db
        Optional<Person> personOption = this.personRepository.findById(response.getIdPerson());
        Assert.assertTrue(personOption.isPresent());

        Person person = personOption.get();
        Assert.assertEquals("12345678T", person.getDocument());
        Assert.assertEquals("Ana Lopez", person.getFullName());
        Assert.assertEquals("desc 3",person.getDescription());
        Assert.assertEquals("13/08/1975",  this.formater.format(person.getBirthDate()));
        Assert.assertEquals(Sex.FAMALE, person.getSex());
    }

    @Test
    public void testUpdate() throws Exception {
        PersonPayload request = new PersonPayload();
        request.setIdPerson(1);
        request.setFullName("Luis Moreno Garcia");
        request.setDocument("00000000T");
        request.setSex(Sex.MALE.getIdSex());
        request.setDescription("desc 1");
        request.setBirthDate(this.getTime());

        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.put("/api/person/1/")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(this.mapToJson(request))).andReturn();
        Assert.assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
        PersonPayload response = this.mapFromJson(mvcResult.getResponse().getContentAsString(), PersonPayload.class);

        Assert.assertEquals(Integer.valueOf(1), response.getIdPerson());
        Assert.assertEquals("00000000T", response.getDocument());
        Assert.assertEquals("Luis Moreno Garcia", response.getFullName());
        Assert.assertEquals("desc 1", response.getDescription());
        Assert.assertEquals("16/09/1980",  this.formater.format(response.getBirthDate()));
        Assert.assertEquals(Sex.MALE, Sex.lookup(response.getSex()));

        //check in db
        Optional<Person> personOption = this.personRepository.findById(response.getIdPerson());
        Assert.assertTrue(personOption.isPresent());

        Person person = personOption.get();
        Assert.assertEquals("00000000T", person.getDocument());
        Assert.assertEquals("Luis Moreno Garcia", person.getFullName());
        Assert.assertEquals("desc 1", person.getDescription());
        Assert.assertEquals("16/09/1980",  this.formater.format(person.getBirthDate()));
        Assert.assertEquals(Sex.MALE, person.getSex());

    }

    private Date getTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1980,8,16);
        return calendar.getTime();
    }

    @Test
    public void testDelete() throws Exception {
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.delete("/api/person//{id}/", 1)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        Assert.assertEquals(HttpStatus.NO_CONTENT.value(), mvcResult.getResponse().getStatus());

        Optional<Person> personOption = this.personRepository.findById(1);
        Assert.assertFalse(personOption.isPresent());
    }
}
