package es.devcenter.talent.campus.restdemo.service;

import es.devcenter.talent.campus.restdemo.dto.InfoPayload;
import es.devcenter.talent.campus.restdemo.properties.RestDemoProperties;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class InfoService {

    private RestDemoProperties restDemoProperties;

    public InfoService(RestDemoProperties restDemoProperties) {
        this.restDemoProperties = restDemoProperties;
    }

    public String getVersion() {
        return this.restDemoProperties.getAnnotation().getVersion();
    }

    public InfoPayload getInfo() {
        InfoPayload infoPayload = new InfoPayload();
        infoPayload.setComment(this.restDemoProperties.getComment());
        infoPayload.getAnnotation().setAuthor(this.restDemoProperties.getAnnotation().getAuthor());
        infoPayload.getAnnotation().setVersion(this.restDemoProperties.getAnnotation().getVersion());

        return infoPayload;
    }
}
