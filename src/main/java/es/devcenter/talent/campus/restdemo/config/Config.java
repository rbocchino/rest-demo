package es.devcenter.talent.campus.restdemo.config;

import es.devcenter.talent.campus.restdemo.properties.RestDemoProperties;
import es.devcenter.talent.campus.restdemo.service.InfoService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories("es.devcenter.talent.campus.restdemo.repository")
@EnableTransactionManagement
public class Config {

    @Bean
    public InfoService infoService(RestDemoProperties restDemoProperties) {
        return new InfoService(restDemoProperties);
    }
}
