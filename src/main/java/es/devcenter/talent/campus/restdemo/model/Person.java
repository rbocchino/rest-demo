package es.devcenter.talent.campus.restdemo.model;

import es.devcenter.talent.campus.restdemo.enums.Sex;
import es.devcenter.talent.campus.restdemo.model.convert.SexConvert;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="person")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_person", updatable = false, nullable = false)
    private Integer idPerson;

    @Column(name = "document", nullable = false, unique = true)
    private String document;

    @Convert(converter = SexConvert.class)
    @Column(name = "sex", nullable = false)
    private Sex sex;

    @Column(name = "birth_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date birthDate;

    @Column(name = "fullname", nullable = false)
    private String fullName;

    @Column(name = "description")
    private String description;


    public Integer getIdPerson() {
        return this.idPerson;
    }

    public void setIdPerson(Integer idPerson) {
        this.idPerson = idPerson;
    }

    public String getDocument() {
        return this.document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public Sex getSex() {
        return this.sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Date getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getFullName() {
        return this.fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
