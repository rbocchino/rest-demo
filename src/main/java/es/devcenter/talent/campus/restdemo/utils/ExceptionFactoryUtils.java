package es.devcenter.talent.campus.restdemo.utils;


import es.devcenter.talent.campus.restdemo.exception.BadRequestException;
import es.devcenter.talent.campus.restdemo.exception.InternalErrorException;
import es.devcenter.talent.campus.restdemo.exception.NotFoundException;

public class ExceptionFactoryUtils {

    private ExceptionFactoryUtils() {}

    public static final InternalErrorException internalErrorException(String message) {
        return new InternalErrorException(message);
    }

    public static final NotFoundException resourceNotFoundException(String message) {
        return new NotFoundException(message);
    }

    public static final BadRequestException badRequestException(String message) {
        return new BadRequestException(message);
    }
}
