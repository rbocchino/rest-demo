package es.devcenter.talent.campus.restdemo.enums;

import java.util.Arrays;
import java.util.Optional;

public enum Sex {

    MALE(0), FAMALE(1);

    private static Sex[] all = {MALE, FAMALE};

    Sex(int idSex) {
        this.idSex = idSex;
    }

    private int idSex;

    public int getIdSex() {
        return this.idSex;
    }

    public static Sex lookup(int value) {
        Optional<Sex> optionalStatus = Arrays.stream(all).filter(status -> status.getIdSex() == value).findFirst();
        if(optionalStatus.isPresent()){
            return optionalStatus.get();
        }

        throw  new IllegalArgumentException("Value not allowed.");
    }
}
