package es.devcenter.talent.campus.restdemo.repository;

import es.devcenter.talent.campus.restdemo.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PersonRepository extends JpaRepository<Person,Integer> {

    Optional<Person> findByDocument(String document);

    List<Person> findByFullNameAndDescriptionIsNotNull(String document);

    boolean existsByDocument(String document);

    @Query("SELECT p FROM Person p WHERE p.fullName LIKE :fullName AND p.description LIKE :desc")
    List<Person> findByFullNameLikeDescription(@Param("fullName") String fullName, @Param("desc") String desc);
}
