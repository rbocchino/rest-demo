package es.devcenter.talent.campus.restdemo.web.rest;

import es.devcenter.talent.campus.restdemo.dto.PersonPayload;
import es.devcenter.talent.campus.restdemo.service.PersonService;
import es.devcenter.talent.campus.restdemo.utils.HttpHeaderUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/person/")
public class PersonResource {

    @Autowired
    private PersonService personService;

    @GetMapping
    public ResponseEntity<List<PersonPayload>> findAll() {
        return new ResponseEntity<>(this.personService.findAll(), HttpStatus.OK);
    }

    @GetMapping(path = "/filter/")
    public ResponseEntity<List<PersonPayload>> getByFilter(@RequestParam(name="fullName") String fullName,
                                                           @RequestParam(name="desc") String desc) {
        return new ResponseEntity<>(this.personService.findByFilter(fullName, desc), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}/")
    public ResponseEntity<PersonPayload> findById(@PathVariable("id") Integer id) {
        return new ResponseEntity<>(this.personService.findPersonById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<PersonPayload> create(@Validated @RequestBody PersonPayload request){
        PersonPayload personPayload = this.personService.create(request);
        return new ResponseEntity<>(personPayload, HttpHeaderUtils.locationHeader(personPayload.getIdPerson()), HttpStatus.CREATED);
    }

    @PutMapping(path = "/{id}/")
    public ResponseEntity<PersonPayload> update(@PathVariable("id") Integer id, @Validated @RequestBody PersonPayload request){
        return new ResponseEntity<>(this.personService.update(id, request), HttpStatus.OK);

    }

    @DeleteMapping(path = "/{id}/")
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id) {
        this.personService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
