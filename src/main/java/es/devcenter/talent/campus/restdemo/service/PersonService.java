package es.devcenter.talent.campus.restdemo.service;

import es.devcenter.talent.campus.restdemo.dto.PersonPayload;
import es.devcenter.talent.campus.restdemo.enums.Sex;
import es.devcenter.talent.campus.restdemo.model.Person;
import es.devcenter.talent.campus.restdemo.repository.PersonRepository;
import es.devcenter.talent.campus.restdemo.utils.ExceptionFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public List<PersonPayload> findAll() {
        return this.personRepository.findAll()
                .stream().map(person -> this.toPersonPayload(person))
                .collect(Collectors.toList());
    }

    public List<PersonPayload> findByFilter(String fullName, String desc) {
        return this.personRepository.findByFullNameLikeDescription("%"+fullName+"%", "%"+desc+"%")
                .stream().map(person -> this.toPersonPayload(person))
                .collect(Collectors.toList());
    }

    private PersonPayload toPersonPayload(Person person) {
        PersonPayload personPayload = new PersonPayload();
        personPayload.setIdPerson(person.getIdPerson());
        personPayload.setDocument(person.getDocument());
        personPayload.setFullName(person.getFullName());
        personPayload.setSex(person.getSex().getIdSex());
        personPayload.setBirthDate(person.getBirthDate());
        personPayload.setDescription(person.getDescription());
        return personPayload;
    }

    public PersonPayload findPersonById(Integer id) {
        Person person = this.findById(id);
        return this.toPersonPayload(person);
    }

    private Person findById(Integer id) {
        if(id == null){
            throw ExceptionFactoryUtils.badRequestException("Id cannot be null");
        }

        Optional<Person> personOptional = this.personRepository.findById(id);
        if( personOptional.isPresent() ){
            return personOptional.get();
        }

        throw ExceptionFactoryUtils.resourceNotFoundException("Cannot found person");
    }

    public void deleteById(Integer id) {
        Person person = this.findById(id);
        this.personRepository.delete(person);
    }

    public PersonPayload create(PersonPayload request) {
        if(this.personRepository.existsByDocument(request.getDocument())) {
            throw ExceptionFactoryUtils.internalErrorException("Document already exists.");
        }

        Person person = this.personRepository.save(this.toPerson(request));
        return this.toPersonPayload(person);
    }

    private Person toPerson(PersonPayload request) {
        Person person = new Person();
        this.savePerson(request, person);
        return person;
    }

    private void savePerson(PersonPayload request, Person person) {
        person.setDocument(request.getDocument());
        person.setFullName(request.getFullName());
        person.setBirthDate(request.getBirthDate());
        person.setSex(Sex.lookup(request.getSex()));
        person.setDescription(request.getDescription());
    }

    public PersonPayload update(Integer id, PersonPayload request) {
        Person person = this.findById(id);
        this.savePerson(request, person);

        Person personResult = this.personRepository.save(person);
        return this.toPersonPayload(personResult);
    }
}
