package es.devcenter.talent.campus.restdemo.model.convert;


import es.devcenter.talent.campus.restdemo.enums.Sex;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;

@Convert
public class SexConvert implements AttributeConverter<Sex, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Sex Sex) {
        if(Sex == null){
            throw  new IllegalArgumentException("Sex value cannot be null");
        }
        return Sex.getIdSex();
    }

    @Override
    public Sex convertToEntityAttribute(Integer value) {
        if(value == null){
            throw  new IllegalArgumentException("Sex cannot be null");
        }
        return Sex.lookup(value);
    }
}
